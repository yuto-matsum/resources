function gitBr {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ [\1]/'
}

PS1="\u@\h:\W\${gitBr}\$ "

alias dir=ls
alias ls='ls -FA'
alias l='ls -lFA'

