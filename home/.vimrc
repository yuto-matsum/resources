set encoding=utf-8
scriptencoding utf-8
set fileencodings=utf-8,euc-jp,cp932
set fileformats=unix,dos,mac

" {{{ dein
" mkdir -p ~/.vim/dein/repos/github.com/Shougo/dein.vim
" git clone https://github.com/Shougo/dein.vim ~/.vim/dein/repos/github.com/Shougo/dein.vim
if &compatible
  set nocompatible
endif
set runtimepath+=~/.vim/dein/repos/github.com/Shougo/dein.vim
call dein#begin(expand('~/.vim/dein'))
call dein#add('junegunn/vim-easy-align')
call dein#add('Shougo/dein.vim')
call dein#add('Shougo/neocomplete.vim')
call dein#add('Shougo/neomru.vim')
call dein#add('Shougo/unite.vim')
call dein#add('Shougo/vimfiler.vim')
call dein#add('tpope/vim-surround')
call dein#add('tpope/vim-pathogen')
call dein#add('altercation/vim-colors-solarized')
call dein#add('itchyny/lightline.vim')
call dein#end()
" dein }}}


" 表示系
set number
set list
set listchars=tab:»-,trail:-,extends:»,precedes:«,nbsp:%,eol:↲
set showmatch
set matchtime=2
set matchpairs& matchpairs+=<:>
set ambiwidth=double

" 画面端

set nowrap
set textwidth=0
set colorcolumn=80
highlight ColorColumn ctermbg=lightgrey guibg=lightgrey

" ステータスライン表示内容
set laststatus=2
set showmode
set showcmd
set ruler
" for itchyny/lightline.vim
let g:lightline = {
  \ 'colorscheme': 'solarized',
  \ }

" 色テーマ
syntax enable
set background=light
colorscheme solarized

" カーソル
set virtualedit=all
augroup vimrc-auto-cursorline
  autocmd!
  autocmd CursorMoved,CursorMovedI,WinLeave * setlocal nocursorline
  autocmd CursorHold,CursorHoldI * setlocal cursorline
augroup END

" mintty & iTerm2
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_SR.="\e[3 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"
" カーソル形状を10msで元に戻す
set ttimeoutlen=10

" netrw
let g:netrw_liststyle = 3
let g:netrw_altv = 1
let g:netrw_alto = 1

" タブ文字
set tabstop=4 shiftwidth=4
set expandtab
set smartindent
set shiftround

" 入力系
set backspace=indent,eol,start

" for vim-easy-align
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" レジスタ、クリップボード
set clipboard=unnamed
" 補完
set infercase

" マウス
set mouse=a

" バッファ切り替え時の自動処理系
set autochdir
set autoread

" バックアップ等
set dir=$TEMP
set backupdir=$TEMP
set undodir=$TEMP

" バッファ利用
set hidden
set switchbuf=useopen

" 検索系
set incsearch
set hlsearch
set ignorecase
set smartcase
nmap <silent> <Esc><Esc> :nohlsearch<CR>

" ウインドウ
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

nnoremap <S-Left>  <C-w><
nnoremap <S-Down>  <C-w>+<CR>
nnoremap <S-Up>    <C-w>-<CR>
nnoremap <S-Right> <C-w>>

" QuickFixをqで閉じる
augroup gfcmd
  autocmd!
  autocmd QuickfixCmdPost make,grep,grepadd,vimgrep copen
  autocmd FileType help,qf nnoremap <buffer> q <C-w>c
augroup END

" 保存
cmap w!! w !sudo tee > /dev/null %

function! s:mkdir(dir, force)
  if !isdirectory(a:dir) && (a:force ||
      \ input(printf('"%s" does not exist. Create? [y/N]', a:dir)) =~?
      \ '^y\%[es]$')
    call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
  endif
endfunction
augroup saving
  autocmd!
  autocmd BufWritePre * call s:mkdir(expand('<afile>:p:h'), v:cmdbang)
augroup END

filetype plugin indent on

