if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

export PATH="/usr/local/sbin:$PATH"

export PATH=./bin:$PATH
export PATH=./node_modules/.bin:$PATH
export PATH=./.cabal-sandbox/bin:../.cabal-sandbox/bin:$HOME/.cabal/bin:$PATH

export EDITOR=vim
export TEMP=/tmp
