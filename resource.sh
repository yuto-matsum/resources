#!/bin/sh

command=$1

if test "$1" = "" -o "$1" = "help" ; then
  cat << _END_
USAGE:
	$0 COMMAND

COMMAND:
	help:	show this message
	deploy:	deploy this resources to your home directory if the source
		is newer than the destination.
	gather:	gather your resources to this directory

_END_

elif test "$1" = "deploy" ; then
  cat catalog.txt | xargs -n 1 -I{} mv ~/{} ~/{}.bak
  cat catalog.txt | xargs -n 1 -I{} cp -Rv home/{} ~/{}

elif test "$1" = "gather" ; then
  cat catalog.txt | xargs -n 1 -I{} cp -fRv ~/{} home/

else
  echo do nothing
fi

